window.addEventListener('DOMContentLoaded', function () {

    var headersDiv = document.createElement('h3')
    headersDiv.className = 'headers-div';
    document.body.appendChild(headersDiv);

    var rocketInfo = (function () {
        /**
         * Creates an instance of a "RocketInfo" on the display
         * @constructor 
         * @param {String} name - The name of the rocket
         * @param {Number} firstStageFuel - Amount of fuel for the first stage
         * @param {Number} secondStageFuel - Amount of fuel for the second stage
         * @param {Number} allFuel - The sum of first and second stage fuel
         */
        class RocketInfo {
            constructor(name, firstStageFuel, secondStageFuel, allFuel) {
                this.name = name;
                this.firstStageFuel = firstStageFuel;
                this.secondStageFuel = secondStageFuel;
                this.allFuel = allFuel;
                this.headers;
                this.clearIntervalId;

                this.start();

            }
            /**Removes the header of each rocket that finishes its fuel*/
            removeHeader() {
                var headers = headersDiv.children;
                var index = Array.prototype.findIndex.call(headers, h => h == this.headers);
                let hDiv = document.querySelector('.headers-div');
                hDiv.removeChild(hDiv.children[index]);
                clearInterval(this.clearIntervalId);
            }

            /** Displays the information from all of the rockets on the side of the screen*/
            showInfo() {
                if (this.firstStageFuel <= 0) {
                    this.firstStageFuel = 0;
                }

                if (!this.headers) {
                    this.headers = document.createElement('h3');
                    headersDiv.appendChild(this.headers);
                    this.headers.innerHTML = `${this.name}</br>First stage fuel <span>${this.firstStageFuel.toFixed(2)}</span>
                    </br> All fuel ${(this.allFuel).toFixed(2)}`;
                } else {
                    if (this.allFuel <= 0) {
                        this.removeHeader();
                    } else {
                        this.headers.innerHTML = '';
                        var f = `${this.name}</br>First stage fuel <span>${this.firstStageFuel.toFixed(2)}</span>
                       </br> All fuel ${this.allFuel.toFixed(2)}`;
                        this.headers.innerHTML = f;
                    }
                }
                this.firstStageFuel--;
                this.allFuel--;
            }

            /**
             * Function that invokes showInfo every second
             */
            start() {
                this.showInfo();
                this.clearIntervalId = setInterval(this.showInfo.bind(this), 1000);
            }
        }
        return RocketInfo;
    })();

    /**Retrieves the information required for every rocket from "rocketService"
     * and sends the data to the constructor of the class "rocketInfo"
     */
    rocketService.getInfo()
        .then((results) => {
            results.forEach(param => {
                new rocketInfo(param.name, param.first_stage, param.second_stage, param.allFuel);
            });
        })
        .catch(err => console.error(err));
});