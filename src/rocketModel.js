window.addEventListener('DOMContentLoaded', function () {

   var rocketsDiv = document.createElement('div');
   rocketsDiv.className = 'rockets-div';
   document.body.appendChild(rocketsDiv);

   var rocketModel = (function () {
      const ROCKET_IMG_TOP = 'assets/images/rocket_top.png';
      const ROCKET_IMG_BOTTOM = 'assets/images/rocket_bottom.png';
      const THURST = 'assets/images/thrust.png';

      /**
       * Creates an instance of a "Rocket" on the display
       * @constructor 
       * @param {Number} firstStageFuel - Amount of fuel for the first stage
       * @param {Number} secondStageFuel - Amount of fuel for the second stage
       * @param {Number} allFuel - The sum of first and second stage fuel
       * @param {Number} maxFuel - The value of the rocket that has most fuel of all rockets
       */
      class Rocket {
         constructor(firstStageFuel, secondStageFuel, allFuel, maxFuel) {
            /**Calculates how many times the rockets display on the screen based on fuel*/
            this.timesToRepeat = maxFuel / 10;

            this.firstStageFuel = firstStageFuel;
            this.secondStageFuel = secondStageFuel;
            this.allFuel = allFuel;

            this.div = this.createRocket();

            this.start();
            this.move();
            this.pulsate();
         }

         /**
          * Function that creates a div with all components 
          * of the rocket during all stages.
          * @returns {Array.<Array-Like Object>} div
          */
         createRocket() {
            var div = document.createElement('div');
            var rocket_top = document.createElement('img')
            var rocket_bottom = document.createElement('img');
            var thurst = document.createElement('img');

            div.className = 'rocket-div';
            rocketsDiv.appendChild(div);

            rocket_top.setAttribute('src', ROCKET_IMG_TOP);

            rocket_bottom.setAttribute('src', ROCKET_IMG_BOTTOM);
            rocket_bottom.setAttribute('id', 'rocket_bottom');

            thurst.setAttribute('id', 'thurst');
            thurst.setAttribute('src', THURST);

            div.appendChild(rocket_top);
            div.appendChild(rocket_bottom);
            div.appendChild(thurst);

            return div;
         }

         /**
          * Function that reloads the page
          */
         showReplay() {
            var button = document.querySelector('button');

            button.innerText = "REPLAY";

            button.addEventListener('click', () => {
               location.reload();
            });
         }

         /**
          * Function that shows a Success button which
          * lasts 3 seconds before calling the showReplay function.
          * It is displayed when the fuel is depleted in all 
          * rockets
          */
         showSuccess() {
            var body = document.body;
            var button = document.createElement('button');

            button.innerText = 'SUCCESS';
            button.setAttribute('id', 'buttonSuccess');
            body.appendChild(button);

            setTimeout(() => {
               this.showReplay();
            }, 3000)
         }

         /**
          * Function that removes rockets when the fuel
          * is depleted with a fadeOut animation and clears
          * the setInterval function of fuelConsummation.
          */
         removeRocketFromPage() {
            TweenMax.to(this.div, 2, {
               opacity: 0
            })
            clearInterval(this.clearIntervalId);

            setTimeout(() => {
               document.querySelector('.rockets-div').removeChild(this.div);

               if (rocketsDiv.children.length === 0) {
                  this.showSuccess();
               }
            }, 2000)

         }

         /**
          * Function that calculates the fuel before removing
          * the rocket when the fuel is empty
          */
         fuelConsummation() {
            if (this.firstStageFuel <= 0) {
               this.firstStageFuel = 0;
               TweenMax.to(this.div.children[1], 2, {
                  opacity: 0
               })

               setTimeout(() => {
                  TweenMax.to(this.div.children[1], .6, {
                     height: 0
                  })
               }, 2002)
            }

            if (this.allFuel <= 0) {
               this.removeRocketFromPage();
            }

            this.firstStageFuel--;
            this.allFuel--;
         }


         /**
          * Function that invokes fuelConsummation every second
          */
         start() {
            this.fuelConsummation();
            this.clearIntervalId = setInterval(this.fuelConsummation.bind(this), 1000);
         }

         /**
          * Function that animates the rockets moving upwards
          */
         move() {
            TweenMax.to('.rocket-div', 10, {
               ease: Power0.easeNone,
               top: 0,
               repeat: this.timesToRepeat,
            })
         }

         /**
          * Function that animates the thrust effect of the rocket
          * so it pulsates
          */
         pulsate() {
            TweenMax.to('#thurst', 1, {
               ease: Circ.easeOut,
               scale: 1,
               repeat: -1,
               yoyo: true
            });
         }

      }
      return Rocket;
   })();

   /**Retrieves the information required for every rocket from "rocketService"
    * and sends the data to the constructor of the class "rocketModel"
    */
   rocketService.getParams()
      .then((results) => {
         /**
          * Function that calculates which rocket has most fuel
          */
         var maxFuel = results.reduce((max, currentValue) => {
            return (max > currentValue.allFuel) ? max : currentValue.allFuel;
         }, results[0]);

         /**
          * Creates the model of the rocket on the page
          */
         results.forEach(param => {
            new rocketModel(param.first_stage, param.second_stage, param.allFuel, maxFuel);
         });

      })
      .catch(err => console.error(err));
});