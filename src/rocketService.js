var rocketService = (function () {

    const apiSpaceX = 'https://api.spacexdata.com/v2/rockets';

    /**
     * Creates an instance of RocketService
     * @param {Array} params - entry point for API parameters.
     * @param {Array} info - entry point for API information.
     */
    class RocketService {
        constructor() {
            this.params = [];
            this.info = [];
        }

        /**
         * Function that fetches and serializes data from the API
         * @returns {Array|Promise} data - data from the API converted to Array 
         */
        getData = async function () {
            let response = await fetch(apiSpaceX);
            let data = await response.json();
            return data;
        }

        /**
         * Function that selects only the needed parameters from 
         * each rocket and sets them in the params array
         * @returns {Array} params 
         */
        getParams = async function () {
            var self = this;
            await this.getData().then(r => {
                r.forEach(r => {
                    self.params.push({
                        first_stage: r.first_stage.fuel_amount_tons,
                        second_stage: r.second_stage.fuel_amount_tons,
                        allFuel: r.first_stage.fuel_amount_tons + r.second_stage.fuel_amount_tons
                    });
                });
            });
            return self.params;
        }

        /**
         * Function that selects only the needed parameters from 
         * each rocket and sets them in the params array
         * @returns {Array} params
         */
        getInfo = async function () {
            var self = this;
            await this.getData().then(r => {
                r.forEach(r => {
                    self.info.push({
                        name: r.name,
                        first_stage: r.first_stage.fuel_amount_tons,
                        second_stage: r.second_stage.fuel_amount_tons,
                        allFuel: r.first_stage.fuel_amount_tons + r.second_stage.fuel_amount_tons
                    });
                })
            })
            return self.info;
        }
    }

    return new RocketService();

})();